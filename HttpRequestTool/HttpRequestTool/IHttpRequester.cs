﻿
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace HttpRequestTool
{

    /// <summary>
    /// 
    /// </summary>
    public interface IHttpRequester
    {
        /// <summary>
        /// Get操作
        /// </summary>
        /// <param name="url">url地址</param>
        /// <param name="param">参数</param>
        /// <param name="cookies">cookie字典</param>
        /// <param name="headers">header字典</param>
        /// <param name="timeout">超时值（以毫秒为单位）</param>
        /// <param name="refererUrl">Referer HTTP 标头的值</param>
        /// <returns></returns>
        HttpWebResponse Get(string url, Dictionary<string, string> param
            , Dictionary<string, string> cookies, Dictionary<string, string> headers, int timeout, string refererUrl);
        /// <summary>
        /// Get操作
        /// </summary>
        /// <param name="url">url地址</param>
        /// <param name="param">参数</param>
        /// <param name="cookies">cookie字典</param>
        /// <param name="headers">header字典</param>
        /// <param name="timeout">超时值（以毫秒为单位）</param>
        /// <param name="encoding">字符编码。默认 UTF8 </param>
        /// <param name="refererUrl">Referer HTTP 标头的值</param>
        /// <returns></returns>
        string Get2(string url, Dictionary<string, string> param
            , Dictionary<string, string> cookies, Dictionary<string, string> headers
            , int timeout, Encoding encoding = null, string refererUrl = null);


        /// <summary>
        /// Post操作
        /// </summary>
        /// <param name="contentType">HttpContentType 枚举</param>
        /// <param name="url">url地址</param>
        /// <param name="param">参数</param>
        /// <param name="cookies">cookie字典</param>
        /// <param name="headers">header字典</param>
        /// <param name="timeout">超时值（以毫秒为单位）默认10分钟 </param>
        /// <param name="encoding">字符编码。默认 UTF8 </param>
        /// <param name="refererUrl">Referer HTTP 标头的值</param>
        /// <returns></returns>
        HttpWebResponse Post(HttpContentType contentType, string url, object param
            , Dictionary<string, string> cookies, Dictionary<string, string> headers
            , int timeout, Encoding encoding, string refererUrl);
        /// <summary>
        /// Post操作
        /// </summary>
        /// <param name="contentType">HttpContentType 枚举</param>
        /// <param name="url">url地址</param>
        /// <param name="param">参数</param>
        /// <param name="cookies">cookie字典</param>
        /// <param name="headers">header字典</param>
        /// <param name="timeout">超时值（以毫秒为单位）默认10分钟 </param>
        /// <param name="encoding">字符编码。默认 UTF8 </param>
        /// <param name="refererUrl">Referer HTTP 标头的值</param>
        /// <returns></returns>
        string Post2(HttpContentType contentType, string url, object param
            , Dictionary<string, string> cookies, Dictionary<string, string> headers
            , int timeout= 600000, Encoding encoding = null, string refererUrl = null);



    }
}
