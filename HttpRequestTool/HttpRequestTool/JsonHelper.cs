﻿using Newtonsoft.Json;

namespace HttpRequestTool
{
    /// <summary>
    /// 
    /// </summary>
    class JsonHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string Serialize(object data) {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(data, settings);
        }








    }
}
