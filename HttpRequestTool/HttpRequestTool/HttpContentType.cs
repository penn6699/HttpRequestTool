﻿
namespace HttpRequestTool
{
    /// <summary>
    /// ContentType 枚举
    /// </summary>
    public enum HttpContentType {

        /// <summary>
        /// application/x-www-form-urlencoded。提交的数据按照 key1=val1&key2=val2 的方式进行编码，key 和 val 都进行了 URL 转码
        /// </summary>
        ApplicationXWwwFormUrlencoded = 1,
        /// <summary>
        /// multipart/form-data。可用于上传文件。
        /// </summary>
        MultipartFormData = 2,
        /// <summary>
        /// application/json
        /// </summary>
        ApplicationJson = 3,
        /// <summary>
        /// text/xml
        /// </summary>
        TextXml = 4




    }

}
